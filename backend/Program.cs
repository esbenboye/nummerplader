﻿using System.Xml;
using Newtonsoft.Json;

using Amazon.S3;
using Amazon.S3.Model;

using System.Data.SQLite;
using System.Net;

// Declare things
string path = "ESStatistikListeModtag.xml";
string startXmlTag = "<ns:Statistik>" ;
string endXmlTag = "</ns:Statistik>" ;
string nameSpace = "ns:";

List<Task> tasks = new();
List<String> fileContent = new();

UInt16 maxTaskQueue = 5000;
UInt64 handledNodes = 0;

StreamReader sr = new StreamReader(path);

double timeDelta = getCurrentTimeStamp();

string xmlData;

logConsole("START");

SqliteHandler dbHandler = new();

do{
        xmlData = readChunk(sr);
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(xmlData);
        try {
            string licensePlate = xmlDoc.SelectSingleNode("/Statistik/RegistreringNummerNummer").InnerText;
            string json = parseRawXmlToJson(xmlData);
            string localHash = md5(json);
            string remoteHash = dbHandler.getEntry(licensePlate);

            if (remoteHash == localHash) {
                continue; // No need to update anything
            }

            tasks.Add(sendToS3(licensePlate, json)); 
            if (remoteHash == null) {
                dbHandler.setEntry(licensePlate, localHash);
            } else {
                dbHandler.updateEntry(licensePlate, localHash);
            }
            // Avoid Out of Memory by chunking the amount of data.
            if(tasks.Count == maxTaskQueue) {
                logConsole($"Accumulated {maxTaskQueue} tasks. Waiting for all tasks to complete");
                Task.WaitAll(tasks.ToArray());
                tasks = new List<Task>(); // Reset tasks List
                logConsole($"{maxTaskQueue} tasks has completed. Handled {handledNodes} nodes");
            }

        } catch (NullReferenceException exception) {
            // This shouldn't happen, but we double check
            logConsole(exception.ToString());
            continue;
        }
}
while(xmlData != "");

logConsole("Waiting for tasks");
Task.WaitAll(tasks.ToArray());
logConsole("Done");

double getCurrentTimeStamp(){
    TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970,1,1,0,0,0);
    return timeSpan.TotalSeconds;
}

string readChunk(StreamReader sr) {
while(sr.Peek() >= 0) {
    string? line = sr.ReadLine();
    if (line == null) {
        continue;
    }

    if (isStartTaggedLine(line, startXmlTag)) {
        fileContent = new List<String>();
        fileContent.Add(line);
    } else if (isEndTaggedLine(line, endXmlTag)) {
        handledNodes++;
        fileContent.Add(line);
        string xmlData = buildXmlData(fileContent, nameSpace);
        // Check if RegistreringNummerNummer exists to prevent NullReferenceException later on
        if(!xmlData.Contains("RegistreringNummerNummer")) {
            continue;
        }

        return xmlData;
    } else {
        fileContent.Add(line);
    }
}
logConsole("Nothing");
return "";
}

void logConsole(String text) {
    Console.WriteLine($"{DateTime.Now}: {text}");
}

string buildXmlData(List<String> fileContent, string nameSpace) {
    return String.Join("", fileContent).Replace(nameSpace, "");
}

string parseRawXmlToJson(string xmlString) {
    XmlDocument xd = new();
    xd.LoadXml(xmlString);
    return JsonConvert.SerializeXmlNode(xd);
}

bool isStartTaggedLine(string line, string startTag) {
    return line.Contains(startTag);
}

bool isEndTaggedLine(string line, string endTag) {
    return line.Contains(endXmlTag);
}

Task sendToS3(string licensePlate, string jsonData) {
    using(var bucket = new AmazonS3Client(Amazon.RegionEndpoint.EUCentral1)){
        PutObjectRequest putRequest = new PutObjectRequest
        {
            BucketName = "www.np-info.dk",
                       Key = $"api/{licensePlate}.json",
                       FilePath = "",
                       ContentType = "application/json",
                       ContentBody = jsonData,
                       CannedACL = S3CannedACL.PublicRead
        };
        return bucket.PutObjectAsync(putRequest);
    }
}

string md5(string input) {
    using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
    {
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hashBytes = md5.ComputeHash(inputBytes);
        return Convert.ToHexString(hashBytes);
    }
}


class SqliteHandler
{
    private Dictionary<string, SQLiteConnection> connections = new();
    public SqliteHandler()
    {
    }

    private void initiateConnection(string letter)
    {
        string dbDir = "dbs";
        if (!Directory.Exists(dbDir)) {
            Directory.CreateDirectory(dbDir);
        }
        string dbPath = $"{dbDir}/{letter}.sqlite";
        bool shouldProvision = !File.Exists(dbPath);
        SQLiteConnection conn = new SQLiteConnection($"Data Source={dbPath}; Version = 3; New = True; Compress = True; ");
        conn.Open();
        if (shouldProvision)
        {
            provision(conn);
        }
        connections.Add(letter, conn);
    }

    private SQLiteConnection getConnection(string licensePlate)
    {
        string letter = licensePlate.Substring(0, 1);
        if (!connections.ContainsKey(letter))
        {
            initiateConnection(letter);
        }

        return connections[letter];
    }

    public string getEntry(string licensePlate)
    {
        SQLiteConnection conn = getConnection(licensePlate);
        string query = $"SELECT hash FROM hashes WHERE license_plate = '{licensePlate}'";
        SQLiteCommand sqlite_cmd = conn.CreateCommand();
        sqlite_cmd.CommandText = query;
        SQLiteDataReader reader = sqlite_cmd.ExecuteReader();

        if (reader == null)
        {
            Console.WriteLine("READER IS NULL");
        }

        while (reader.Read())
        {
            return reader.GetString(0);
        }

        return null;

    }

    public bool updateEntry(string licensePlate, string hash)
    {
        SQLiteConnection conn = getConnection(licensePlate);
        string query = $"UPDATE hashes SET `hash` = '{hash}' WHERE license_plate = '{licensePlate}'";
        SQLiteCommand sqlite_cmd = conn.CreateCommand();
        sqlite_cmd.CommandText = query;
        return sqlite_cmd.ExecuteNonQuery() > 0;
    }

    public bool setEntry(string licensePlate, string hash)
    {
        SQLiteConnection conn = getConnection(licensePlate);
        string query = $"INSERT INTO hashes(`license_plate`,`hash`) VALUES('{licensePlate}','{hash}')";
        SQLiteCommand sqlite_cmd = conn.CreateCommand();
        sqlite_cmd.CommandText = query;
        return sqlite_cmd.ExecuteNonQuery() > 0;
    }

    private void provision(SQLiteConnection conn)
    {
        SQLiteCommand cmd = conn.CreateCommand();
        string query = "CREATE TABLE \"hashes\" (\"id\" integer, \"license_plate\" varchar(8), \"hash\" varchar(32), PRIMARY KEY (id));";
        cmd.CommandText = query;
        cmd.ExecuteNonQuery();
    }
}