#!/bin/bash
FTP_PATH=ftp://5.44.137.84/ESStatistikListeModtag/
[ -f "*.zip*" ] && rm *.zip*
wget --no-remove-listing -q $FTP_PATH 
FILE_PATH=$(cat .listing | tail -n1 | awk '{print $9}' | tr -d '\r')

[ -f ".listing" ] && rm .listing
[ -f "index.html" ] && rm index.html

echo "GETTING $FTP_PATH$FILE_PATH"
wget "$FTP_PATH$FILE_PATH"

unzip $FILE_PATH

dotnet run
